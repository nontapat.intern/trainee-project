from pymongo import MongoClient

def connect_to_database(mongo_uri, db_name):
    client = MongoClient(mongo_uri)
    db = client[db_name]
    return db

def profile_uris_collection(db):
    return db['profile_uris']

def categories_collection(db):
    return db['categories']

def bookmarks_collection(db):
    return db['bookmarks']

def followers_collection(db):
    return db['followers']

def validate_data_before_insert_url(url, title, link, channel_id):
    if url and title and link and channel_id:
        return True
    else:
        return False
    
def validate_data_before_insert_bookmark(url, title, user_id):
    if url and title and user_id:
        return True
    else:
        return False

        

