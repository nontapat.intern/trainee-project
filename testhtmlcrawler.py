import aiohttp
import asyncio
from bs4 import BeautifulSoup
from datetime import datetime
import xml.etree.ElementTree as ET

async def fetch_html_async(session, url):
    try:
        async with session.get(url) as response:
            response.raise_for_status()
            return await response.text()
    except aiohttp.ClientError as e:
        print(f"Failed to fetch HTML from {url}. Error: {e}")
        return None

async def process_url_async(session, url):
    print(f"Processing URL: {url}")
    html = await fetch_html_async(session, url)

    if html is not None:
        soup = BeautifulSoup(html, "lxml")
        buttons_inside_bl_span = soup.select('div.bl button')

        if buttons_inside_bl_span:
            numeric_count = sum(int(button.text.strip()) for button in buttons_inside_bl_span if button.text.strip().isdigit())
            print(f"Total Numeric Count for {url}: {numeric_count}")
            return numeric_count
        else:
            print(f"ไม่พบ <button> ที่อยู่ภายใน <div class='bl'> และ <span> ใน URL: {url}")
    return 0

async def get_url_async(session, link):
    try:
        async with session.get(link) as response:
            response.raise_for_status()
            return await response.text()
    except aiohttp.ClientError as e:
        print(f"Failed to fetch data from {link}. Error: {e}")
        return None

async def get_popular_blog_from_profile_async(link, min_items=1, max_items=10):
    async with aiohttp.ClientSession() as session:
        html = await get_url_async(session, link)

        try:
            root = ET.fromstring(html)
        except ET.ParseError as e:
            print(f"Failed to parse XML data. Error: {e}")
            return None

        items = []

        for item_elem in root.findall('.//item'):
            title = item_elem.findtext('title', default='N/A').strip()
            link = item_elem.findtext('link', default='N/A').strip()
            pub_date_str = item_elem.findtext('pubDate', default='').strip()

            if not pub_date_str:
                print(f"Skipping item with missing pubDate: {title}")
                continue

            try:
                pub_date = datetime.strptime(pub_date_str, '%a, %d %b %Y %H:%M:%S %Z')
            except ValueError as e:
                print(f"Failed to parse pubDate for {title}. Error: {e}")
                continue

            items.append({
                'title': title,
                'link': link,
                'pub_date': pub_date
            })

        num_items_to_process = min(max(len(items), min_items), max_items)

        tasks = [process_url_async(session, item['link']) for item in items[:num_items_to_process]]
        numeric_counts = await asyncio.gather(*tasks)

    max_numeric_count = max(numeric_counts, default=0)
    url_with_max_numeric_count = items[numeric_counts.index(max_numeric_count)]['link'] if max_numeric_count > 0 else None

    return url_with_max_numeric_count

async def main():
    link = "https://medium.com/feed/@sheilateozy"
    result = await get_popular_blog_from_profile_async(link, min_items=1, max_items=10)
    print("URL with maximum numeric count:", result)

if __name__ == "__main__":
    asyncio.run(main())
