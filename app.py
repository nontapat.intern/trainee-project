import requests
import xml.etree.ElementTree as ET
from datetime import datetime
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
from urllib.parse import urlparse
from apscheduler.schedulers.background import BackgroundScheduler
import atexit
from database import connect_to_database, profile_uris_collection, categories_collection
from bs4 import BeautifulSoup
import asyncio
import aiohttp

MONGO_URI = 'mongodb+srv://nontapatb:110020103431a@trainee-bot.fnniy7q.mongodb.net/?retryWrites=true&w=majority'
DB_NAME = 'Test4'

db = connect_to_database(MONGO_URI, DB_NAME)

def get_url(link):
    try:
        response = requests.get(link)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        print(f"Failed to fetch data from {link}. Error: {e}")
        return None, None 

    try:
        root = ET.fromstring(response.text)
    except ET.ParseError as e:
        print(f"Failed to parse XML data. Error: {e}")
        print("Response Text:", response.text)
        return None, None

    items = []

    for item_elem in root.findall('.//item'):
        title = item_elem.find('title').text.strip()
        link = item_elem.find('link').text.strip()
        pub_date_str = item_elem.find('pubDate').text.strip()

        pub_date = datetime.strptime(pub_date_str, '%a, %d %b %Y %H:%M:%S %Z')

        items.append({
            'title': title,
            'link': link,
            'pub_date': pub_date
        })
    return items

def get_latest_blog(link):
    items = get_url(link)
    sorted_items = sorted(items, key=lambda x: x['pub_date'], reverse=True)

    if sorted_items:
        latest_item = sorted_items[0]
        return latest_item, latest_item['link']
    else:
        print("No items found in the XML data.")
        return None, None

def send_slack_message(channel, message):
    client = WebClient(token='xoxb-6394601507699-6409135828614-vs0uNfx43Aq2uFfRSRho9doR')
    try:
        response = client.chat_postMessage(
            channel=channel,
            text=message
        )
        print("Message sent to Slack successfully:", response['ts'])
    except SlackApiError as e:
        print(f"Failed to send message to Slack. Error: {e.response['error']}")
    

def normalize_uri(url, type):
    if type == 'profile':
        parsed_url = urlparse(url)
        if parsed_url.netloc == 'medium.com':
            if '/@' in parsed_url.path:
                username = parsed_url.path.split('/@')[1]
                normalized_url = f"https://medium.com/feed/@{username}"
            elif parsed_url.path.startswith('/'):
                username = parsed_url.netloc.split('.')[0]
                normalized_url = f"https://medium.com/feed/@{username}"
            else:
                normalized_url = url
        elif '.medium.com' in parsed_url.netloc:
            username = parsed_url.netloc.split('.')[0]
            normalized_url = f"https://medium.com/feed/@{username}"
        else:
            normalized_url = url
        return normalized_url
    elif type == 'category':
        normalized_url = f'https://medium.com/feed/tag/{url}'
        return normalized_url
    
def fetch_html(url):
    try:
        response = requests.get(url)
        response.raise_for_status()
        return response.text
    except requests.exceptions.RequestException as e:
        print(f"Failed to fetch HTML from {url}. Error: {e}")
        return None

def process_url(url):
    print(f"Processing URL: {url}")
    html = fetch_html(url)

    if html is not None:
        soup = BeautifulSoup(html, "lxml")
        buttons_inside_bl_span = soup.select('div.bl button')

        if buttons_inside_bl_span:
            numeric_count = sum(int(button.text.strip()) for button in buttons_inside_bl_span if button.text.strip().isdigit())
            print(f"Total Numeric Count for {url}: {numeric_count}")
            return numeric_count
        else:
            print(f"ไม่พบ <button> ที่อยู่ภายใน <div class='bl'> และ <span> ใน URL: {url}")
    return 0

async def fetch_html_async(session, url):
    try:
        async with session.get(url) as response:
            response.raise_for_status()
            return await response.text()
    except aiohttp.ClientError as e:
        print(f"Failed to fetch HTML from {url}. Error: {e}")
        return None

async def process_url_async(session, url):
    print(f"Processing URL: {url}")
    html = await fetch_html_async(session, url)

    if html is not None:
        soup = BeautifulSoup(html, "lxml")
        buttons_inside_bl_span = soup.select('div.bl button')

        if buttons_inside_bl_span:
            numeric_count = sum(int(button.text.strip()) for button in buttons_inside_bl_span if button.text.strip().isdigit())
            print(f"Total Numeric Count for {url}: {numeric_count}")
            return numeric_count
        else:
            print(f"ไม่พบ <button> ที่อยู่ภายใน <div class='bl'> และ <span> ใน URL: {url}")
    return 0

async def get_popular_blog_from_profile(link, min_items=1, max_items=10):
    async with aiohttp.ClientSession() as session:
        items = get_url(link)
        sorted_items = sorted(items, key=lambda x: x['pub_date'], reverse=True)
        num_items_to_process = min(max(len(sorted_items), min_items), max_items)

        tasks = [process_url_async(session, item['link']) for item in items[:num_items_to_process]]
        numeric_counts = await asyncio.gather(*tasks)

    max_numeric_count = max(numeric_counts, default=0)
    url_with_max_numeric_count = items[numeric_counts.index(max_numeric_count)]['link'] if max_numeric_count > 0 else None

    return url_with_max_numeric_count
    
    
scheduler = BackgroundScheduler(daemon=True)

def search_and_send():
    # Process links
    profile_urls = profile_uris_collection(db)

    for document in profile_urls.find():
        profile_url = document['profile_url']
        channel_id = document['channel_id']
        normalize_profile_url = normalize_uri(profile_url, 'profile')
        # Fetch the latest item for the given URL
        latest_item, new_latest_link_url = get_latest_blog(normalize_profile_url)
        if latest_item and new_latest_link_url:
            # Check if there is an update
            if new_latest_link_url != document['latest_blog_link']:
                # Update the latest_link_url in the database
                profile_urls.update_one({'profile_url': profile_url, 'channel_id': channel_id}, {'$set': {'latest_blog_link': new_latest_link_url}})

                # Send a message to Slack only if it hasn't been sent before
                if document['latest_blog_link'] != new_latest_link_url:
                    # Send a message to Slack
                    send_slack_message(channel_id, f"Latest blog: {latest_item['title']}\nLink: {new_latest_link_url}\nYou can delete by command /delete {profile_url}")
            else:
                # Notify if there is no update in the blog
                print(f"No update in blog: {profile_url}")
                
    # Process categories
    categories = categories_collection(db)

    for document in categories.find():
        category = document['category_url']
        channel_id = document['channel_id']
        normalize_category_url = normalize_uri(category, 'category')
        
        # Fetch the latest item for the given category URL
        latest_item, new_latest_link_url = get_latest_blog(normalize_category_url)
        if latest_item is not None:
            # Check if there is an update
            if new_latest_link_url != document['latest_blog_link']:
                # Update the latest_link_url in the database
                categories.update_one({'category_url': category, 'channel_id': channel_id}, {'$set': {'latest_blog_link': new_latest_link_url}})

                # Send a message to Slack only if it hasn't been sent before
                if document['latest_blog_link'] != new_latest_link_url:
                    # Send a message to Slack
                    send_slack_message(channel_id, f"Latest blog in category {category}:\nTitle: {latest_item['title']}\nLink: {new_latest_link_url}\nYou can delete by command /delete {category}")
            else:
                # Notify if there is no update in the category
                print(f"No update in category: {category}")

scheduler.add_job(search_and_send, 'interval', seconds=30)

scheduler.start()

atexit.register(lambda: scheduler.shutdown())