from flask import Flask, request, jsonify
from database import connect_to_database, profile_uris_collection, categories_collection, bookmarks_collection, validate_data_before_insert_bookmark, validate_data_before_insert_url
from app import normalize_uri, get_latest_blog, send_slack_message, get_latest_blog, get_popular_blog_from_profile
from urllib.parse import urlparse, unquote
import json
import aiohttp

from flask import request, jsonify

MONGO_URI = 'mongodb+srv://nontapatb:110020103431a@trainee-bot.fnniy7q.mongodb.net/?retryWrites=true&w=majority'
DB_NAME = 'Test4'

app = Flask(__name__)

db = connect_to_database(MONGO_URI, DB_NAME)
profile_collection = profile_uris_collection(db)
category_collection = categories_collection(db)
bookmark_collection = bookmarks_collection(db)

@app.route('/health', methods=['GET'])
def health_check():
    return jsonify(status='OK')

@app.route('/slack/add', methods=['POST'])
def slack_add_command():
    input_text = request.form['text']
    payload = request.form.to_dict()
    print(payload)
    channel_id = payload.get('channel_id', None)

    if channel_id:
        if urlparse(input_text).scheme in ('http', 'https'):
            
            if profile_collection.find_one({'profile_url': input_text, 'channel_id' : channel_id}):
                response_text = f"*Category* {input_text} *already exists in the list*"
            else:
                normalize_profile_uri = normalize_uri(input_text, 'profile')
                latest_item, new_latest_link_url = get_latest_blog(normalize_profile_uri)
                if validate_data_before_insert_url(input_text, latest_item['title'], new_latest_link_url, channel_id):
                    profile_collection.insert_one({'profile_url' : input_text, 'title': latest_item['title'],'latest_blog_link': new_latest_link_url, 'channel_id': channel_id})
                    response_text = f'Link {input_text} added and verified successfully'
                    send_slack_message(channel_id,  f"*Latest blog:* {latest_item['title']}\n*Link:* {new_latest_link_url}\n*You can delete by command* /delete {input_text}")
                else:
                    return print("Invalid data.")
        else:
    
            if category_collection.find_one({'category_url': input_text.lower(), 'channel_id' : channel_id}):
                response_text = f"Category {input_text} already exists in the list"
            else:
                normalize_category_uri = normalize_uri(input_text, 'category')
                latest_item, new_latest_link_url = get_latest_blog(normalize_category_uri)
                if validate_data_before_insert_url(input_text.lower(),latest_item['title'],  new_latest_link_url, channel_id):
                    category_collection.insert_one({'category_url' : input_text.lower(),'title': latest_item['title'], 'latest_blog_link': new_latest_link_url, 'channel_id': channel_id})
                    response_text = f'Link {input_text} added and verified successfully'
                    send_slack_message(channel_id,  f"*Latest blog:* {latest_item['title']}\n*Link:* {new_latest_link_url}\n*You can delete by command* /delete {input_text}")
                else:
                    return print("Invalid data.")

        return jsonify(text=response_text)
    else:
        response_text = f'*Can not find channel ID :* {channel_id}'
        return jsonify(text= response_text)

from bson import ObjectId

@app.route('/slack/interactive', methods=['POST'])
async def slack_interactive():
    payload = json.loads(request.form['payload'])
    action_name = payload['actions'][0]['name']

    if action_name == 'delete':
        return slack_delete_interactive(payload)
    elif action_name == 'bookmark':
        return slack_bookmark_interactive(payload)
    elif action_name == 'popular':
        return await slack_popular_interactive(payload)
    else:
        return jsonify(text="Invalid action")
    
def slack_delete_interactive(payload):
    # ดึงค่า ObjectId ที่ต้องการลบ
    object_id_to_delete = payload['actions'][0]['value']

    # ลบข้อมูลจาก collection ตามประเภทของข้อมูล
    if profile_collection.find_one({'_id': ObjectId(object_id_to_delete)}):
        profile_collection.delete_one({'_id': ObjectId(object_id_to_delete)})
        response_text = f"*Profile deleted successfully*"
    elif category_collection.find_one({'_id': ObjectId(object_id_to_delete)}):
        category_collection.delete_one({'_id': ObjectId(object_id_to_delete)})
        response_text = f"*Category deleted successfully*"
    elif bookmark_collection.find_one({'_id': ObjectId(object_id_to_delete)}):
        bookmark_collection.delete_one({'_id': ObjectId(object_id_to_delete)})
        response_text = f"*Bookmark deleted successfully*"
    else:
        response_text = f"Object with ObjectId {object_id_to_delete} not found in the list"

    return jsonify(text=response_text)

def slack_bookmark_interactive(payload):
    # ดึงข้อมูลที่ต้องการ bookmark
    bookmark_value = unquote(payload['actions'][0]['value'])
    user_id = payload['user']['id']  # ดึง channel_id จาก payload
    print(bookmark_value)
    blog_url = bookmark_value.split('___')[0]
    title = bookmark_value.split('___')[1]
    if validate_data_before_insert_bookmark(blog_url, title, user_id):
        bookmark_collection.insert_one({
            'blog_url': blog_url,
            'title': title,
            'user_id': user_id
        })

        response_text = f"*Link bookmarked successfully:* {title}"
    else:
        response_text = f"Invalid data."

    return jsonify(text=response_text)

async def slack_popular_interactive(payload):
    value = payload['actions'][0]['value']
    link = value.split('___')[0]
    title_type = value.split('___')[1]

    if title_type == 'profile':
        normalize_profile_uri = normalize_uri(link, title_type)

        # ส่ง "Processing..." ไปที่ Slack ก่อนที่จะทำงาน
        response_text = 'Processing...'
        await send_to_slack(response_text, payload['response_url'])

        # ทำงานที่ต้องการ
        popular_blog = await get_popular_blog_from_profile(normalize_profile_uri)
        print(popular_blog)

        # ส่งข้อความไปที่ Slack หลังจากที่ popular_blog ถูกคำนวณและเสร็จสมบูรณ์
        response_text = f'The most popular blog from {link} is {popular_blog}'
        await send_to_slack(response_text, payload['response_url'])

    # คืนค่า response_text
    return jsonify(text=response_text), 200, {'Content-Type': 'application/json'}

# สร้างฟังก์ชัน send_to_slack เพื่อส่งข้อความไปยัง Slack
async def send_to_slack(response_text, response_url):
    async with aiohttp.ClientSession() as session:
        await session.post(response_url, json={"text": response_text})

@app.route('/slack/show', methods=['POST'])
def slack_show_command():
    channel_id = request.form['channel_id']
    
    # Retrieve profile URLs and categories for the given channel_id
    profile_urls = profile_uris_collection(db).find({'channel_id': channel_id})
    category_urls = categories_collection(db).find({'channel_id': channel_id})

    # Create an interactive message with buttons to delete and bookmark each item
    attachments = []

    if profile_collection.find_one({'channel_id' : channel_id}) or category_collection.find_one({'channel_id' : channel_id}):
        for profile in profile_urls:
            latest_link= profile.get('latest_blog_link', 'No latest link available')
            title = profile.get('title', 'No title availiable')

            attachments.append({
                "text": f"*From:* {profile['profile_url']} \n*Latest update blog:* <{latest_link}| {profile['title']}>",
                "fallback": "Manage",
                "callback_id": "manage_button",
                "color": "#F2F",
                "attachment_type": "default",
                "actions": [
                    {
                        "name": "bookmark",
                        "text": "Bookmark",
                        "type": "button",
                        "value": f'{latest_link}___{title}',
                        "style": "primary"
                    },
                    {
                        "name": "popular",
                        "text": "Popular",
                        "type": "button",
                        "value": f"{profile['profile_url']}___profile",
                    },
                    {
                        "name": "delete",
                        "text": "Delete",
                        "type": "button",
                        "value": str(profile['_id']),
                        "style": "danger"
                    }
                ]
            })

        for category in category_urls:
            latest_link = category.get('latest_blog_link', 'No latest link available')
            title = category.get('title', 'No title availiable')

            attachments.append({
                "text": f"*From category:* {category['category_url']} \n*Latest update blog:* <{latest_link}| {category['title']}>",
                "fallback": "Manage",
                "callback_id": "manage_button",
                "color": "#3BF7EC",
                "attachment_type": "default",
                "actions": [
                    
                    {
                        "name": "bookmark",
                        "text": "Bookmark",
                        "type": "button",
                        "value": f'{latest_link}___{title}',
                        "style": "primary"
                    },
                    {
                        "name": "popular",
                        "text": "Popular",
                        "type": "button",
                        "value": f"{category['category_url']}___category",
                    },
                    {
                        "name": "delete",
                        "text": "Delete",
                        "type": "button",
                        "value": str(category['_id']),
                        "style": "danger"
                    }
                ]
            })

        response = {
            "text": "*---Profile URLs and Categories---*",
            "attachments": attachments
        }

        # Set the Content-Type header to application/json
        return jsonify(response), 200, {'Content-Type': 'application/json'}
    else:
        response = {
            "text": "No bookmark founded, You can add your bookmark by using command /show",
            "attachments": attachments
        }
        return jsonify(response), 200, {'Content-Type': 'application/json'}
    
@app.route('/slack/bookmarks', methods=['POST'])
def slack_show_bookmarks_command():
    payload = request.form.to_dict()
    user_id = payload.get('user_id', None)
    user_name = payload.get('user_name', None)
    # Retrieve bookmarks for the given user_id
    bookmarks = bookmark_collection.find({'user_id': user_id})

    if not bookmark_collection.find_one({'user_id' : user_id}):
        response = {
            "text": f"*No Bookmarks found in user* {user_name}"
        }
    else:
        # Create an interactive message with buttons to delete each bookmark
        attachments = []

        for bookmark in bookmarks:
            attachments.append({
                
                "text": f"*Title* \n<{bookmark['blog_url']}| {bookmark['title']}>",
                "fallback": "Delete",
                "callback_id": "delete_bookmark_button",
                "color": "#FAFF17",
                "attachment_type": "default",
                "actions": [
                    {
                        "name": "delete",
                        "text": "Delete",
                        "type": "button",
                        "value": str(bookmark['_id']),
                        "style": "danger"
                    }
                ]
            })

        response = {
            "text": f"*Bookmarks for user* {user_name}",
            "attachments": attachments
        }


    return jsonify(response), 200, {'Content-Type': 'application/json'}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

# @app.route('/slack/follower', methods=['POST'])
# def slack_follower_command():
#     payload = request.form.to_dict()
#     channel_id = payload.get('channel_id', None)
